output "id" {
  value = openstack_compute_instance_v2.ctrl.id
}

output "ctrl_port_id" {
  value = openstack_networking_port_v2.ctrl.id
}
