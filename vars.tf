#
# Required
#
variable "vpod_name" {
  type = string
}

variable "ctrl_network_id" {
  type = string
}

variable "ctrl_subnet_id" {
  type = string
}

variable "rnic_network_id" {
  type = string
}

variable "rnic_subnet_id" {
  type = string
}

variable "storage_network_id" {
  type = string
}

variable "storage_subnet_id" {
  type = string
}

variable "assigned_ipums" {
  type = list(string)
}

variable "cidr_hint" {
}

#
# Optional
#
variable "dns_base" {
  type    = string
  default = "openstack.example.net"
}

variable "flavor" {
  type    = string
  default = "r640.tiny"
}

variable "image" {
  type    = string
  default = "ubuntu-18.04-mlnx"
}

variable "key_pair" {
  type    = string
  default = "seed"
}

variable "ipum_network_name" {
  type    = string
  default = "ipum-1g"
}

variable "bmc_subnet_name" {
  type    = string
  default = "ipum-1g-bmc"
}

variable "gw_subnet_name" {
  type    = string
  default = "ipum-1g-gw"
}

variable "oob_mgt_host" {
  type = string
  default = "1.2.3.4"
}
