# vpod-ctrl
Terraform Module: vpod-ctrl
===========================

Overview
--------

This module is used to create a virtual instance for running vPOD 'control' activities, including running the V-IPU server for the vPOD.  The default size for the control instance is an r640.tiny, and it will be placed into the general purpose (general) availability zone (ie, not on a Poplar host).

Networking
----------
The vpod-ctrl instance is connected to:
* Common IPUM BMC network
* Common IPUM GW networks
* Dedicated vPOD control network (10.3.0.0/24)
* Dedicated vPOD RNIC network (10.5.0.0/16)
* Dedicated vPOD storage network (10.12.X.0/24)

Prerequisites
-------------
* This assumes that an NFS filesystem /admin1 is available on 10.12.{cidr_hint}.250.   This is added to /etc/fstab, mounting to /mnt/admin
* This assumes that an NFS filesystem /public1 is available on 10.12.{cidr_hint}.250.   This is added to /etc/fstab, mounting to /mnt/public
* There is an 'out of band' management instance, managed outside of this vPOD.   Security groups to allow access from this out-of-band system are created, and the variable 'oob_mgt_host' is used to set that IP. 

Access Control
--------------
Security groups are used to control egress from the vpod-ctrl instance on the common BMC/GW network so that they can access ONLY their assigned IPUMs.  In future this will be superceded by dedicated BMC/GW networks, or network RBAC. 

Additional security groups are configured to allow for Prometheus monitoring access. 

Usage
-----

    module "vpod-ctrl" {
    source                = "git::ssh://git@gitlab.com/gc-openstack/terraform-modules/vpod-ctrl.git"
    vpod_name             = var.vpod_name
    ctrl_network_id       = module.networks.ctrl_network_id
    ctrl_subnet_id        = module.networks.ctrl_subnet_id
    rnic_network_id       = module.networks.rnic_network_id
    rnic_subnet_id        = module.networks.rnic_subnet_id
    storage_network_id    = module.networks.storage_network_id
    storage_subnet_id     = module.networks.storage_subnet_id
    assigned_ipums        = var.assigned_ipums
    cidr_hint             = var.cidr_hint
    image                 = var.image
    }

where the pod vars are:

    vpod_name         = "vpod7"
    ctrl_vlan         = 1234
    rnic_vlan         = 2345
    storage_vlan      = 3456
    cidr_hint         = 100
    ipum_csv          = "rnic_rack8.csv"
    vpod_floatingip   = "1.2.3.4"
    poplaraz          = "rack8-AZ"
    assigned_ipums    = ["rack8-ipum1", "rack8-ipum2", "rack8-ipum3", "rack8-ipum4", "rack8-ipum5", "rack8-ipum6", "rack8-ipum7", "rack8-ipum8", "rack8-ipum9", "rack8-ipum10", "rack8-ipum11", "rack8-ipum12", "rack8-ipum13", "rack8-ipum14", "rack8-ipum15", "rack8-ipum16" ]
    flavor            = "r6525.full" # flavor used for Poplar hosts only
    image             = "ubuntu-18.04-mlnx"

Optional Variables
------------------
* dns_base: default = "openstack.example.net"
* flavor: default = "r640.tiny"
* image: default = "ubuntu-18.04-mlnx"
* key_pair: default = "seed"

Cloud Init Actions
------------------
A cloud-init config file is templated to user-data, and handed to instance at creation.   This performs the following actions:
* Addition of NFS filesystem mounts to /etc/fstab
* Installation of useful packages
* Templating of systemd-networkd files to disable DNS servers provided via DHCP, leaving DNS available only on 10.3.X.0/24.   This will no longer be required following planned DNS enhancements.
* Restart of systemd-networkd, following the above action.  

Development and Feedback
========================
The development of this tooling is ongoing, and any feedback is appreciated.  If you find any errors or bugs, please create an issue on the appropriate repository, or contact your Graphcore representative.