terraform {
  required_version = ">= 1.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = ">= 1.35"
    }
  }
}

locals {
  name = "${var.vpod_name}-ctrl"
}

resource "openstack_networking_secgroup_v2" "ctrl" {
  name        = local.name
  description = "Security group for vipu controller"
}

# Expose VIPU Controller User API only on ctrl network
resource "openstack_networking_secgroup_rule_v2" "vipu" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 8090
  port_range_max    = 8090
  remote_ip_prefix  = "10.3.0.0/16"
  security_group_id = openstack_networking_secgroup_v2.ctrl.id
}

# Expose ssh via all IPv4 addresses
resource "openstack_networking_secgroup_rule_v2" "vipu_ssh" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.ctrl.id
}

resource "openstack_networking_secgroup_v2" "ctrl-1g" {
  name        = "${local.name}-1G"
  description = "Security group for vipu controller on the 1G network"
  delete_default_rules = true
}

# get the IP addresses of the assigned IPUM GWs
data "openstack_networking_port_v2" "ipum-gws" {
  for_each = toset(var.assigned_ipums)
  name = "${each.key}gw"
}

resource "openstack_networking_secgroup_rule_v2" "vipu-ipum" {
  for_each = data.openstack_networking_port_v2.ipum-gws

  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 8080
  port_range_max    = 8080
  remote_ip_prefix  = "${element(each.value.all_fixed_ips, 0)}/32"
  security_group_id = openstack_networking_secgroup_v2.ctrl-1g.id
}

resource "openstack_networking_secgroup_rule_v2" "ssh-ipum" {
  for_each = data.openstack_networking_port_v2.ipum-gws

  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "${element(each.value.all_fixed_ips, 0)}/32"
  security_group_id = openstack_networking_secgroup_v2.ctrl-1g.id
}

resource "openstack_networking_secgroup_rule_v2" "icmp-ipum" {
  for_each = data.openstack_networking_port_v2.ipum-gws

  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "icmp"
  port_range_min    = 0
  port_range_max    = 0
  remote_ip_prefix  = "${element(each.value.all_fixed_ips, 0)}/32"
  security_group_id = openstack_networking_secgroup_v2.ctrl-1g.id
}

resource "openstack_networking_secgroup_rule_v2" "gw-net-dns-tcp" {
  for_each = toset(["100", "101"])
  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 53
  port_range_max    = 53
  remote_ip_prefix = "10.2.255.${each.key}/32"
  security_group_id = openstack_networking_secgroup_v2.ctrl-1g.id
}

resource "openstack_networking_secgroup_rule_v2" "gw-net-dns-udp" {
  for_each = toset(["100", "101"])
  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "udp"
  port_range_min    = 53
  port_range_max    = 53
  remote_ip_prefix = "10.2.255.${each.key}/32"
  security_group_id = openstack_networking_secgroup_v2.ctrl-1g.id
}

resource "openstack_networking_secgroup_rule_v2" "gw-monitoring" {
  for_each = data.openstack_networking_port_v2.ipum-gws
  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 2112
  port_range_max    = 2112
  remote_ip_prefix  = "${element(each.value.all_fixed_ips, 0)}/32"
  security_group_id = openstack_networking_secgroup_v2.ctrl-1g.id
}

resource "openstack_networking_secgroup_rule_v2" "gw-ntp" {
  for_each = data.openstack_networking_port_v2.ipum-gws
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "udp"
  port_range_min    = 123
  port_range_max    = 123
  remote_ip_prefix  = "${element(each.value.all_fixed_ips, 0)}/32"
  security_group_id = openstack_networking_secgroup_v2.ctrl-1g.id
}

resource "openstack_networking_secgroup_rule_v2" "gw-rsyslog" {
  for_each = data.openstack_networking_port_v2.ipum-gws
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "udp"
  port_range_min    = 514
  port_range_max    = 514
  remote_ip_prefix  = "${element(each.value.all_fixed_ips, 0)}/32"
  security_group_id = openstack_networking_secgroup_v2.ctrl-1g.id
}

resource "openstack_networking_secgroup_rule_v2" "oob-ssh" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "${var.oob_mgt_host}/32"
  security_group_id = openstack_networking_secgroup_v2.ctrl-1g.id
}

resource "openstack_networking_secgroup_rule_v2" "oob-monitoring" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 9090
  port_range_max    = 9090
  remote_ip_prefix  = "${var.oob_mgt_host}/32"
  security_group_id = openstack_networking_secgroup_v2.ctrl-1g.id
}

resource "openstack_networking_secgroup_rule_v2" "oob-grafana" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 3000
  port_range_max    = 3000
  remote_ip_prefix  = "${var.oob_mgt_host}/32"
  security_group_id = openstack_networking_secgroup_v2.ctrl-1g.id
}

resource "openstack_networking_secgroup_rule_v2" "oob-vipu-mon" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 2113
  port_range_max    = 2113
  remote_ip_prefix  = "${var.oob_mgt_host}/32"
  security_group_id = openstack_networking_secgroup_v2.ctrl-1g.id
}

resource "openstack_networking_port_v2" "ctrl" {
  name               = local.name
  network_id         = var.ctrl_network_id
  admin_state_up     = "true"
  security_group_ids = [openstack_networking_secgroup_v2.ctrl.id]
  fixed_ip {
    subnet_id = var.ctrl_subnet_id
  }
}

resource "openstack_networking_secgroup_v2" "ctrl-1g-bmc" {
  name        = "${local.name}-1G-bmc"
  description = "Security group for vipu controller on the 1G BMC network"
  delete_default_rules = true
}

# get the IP addresses of the assigned IPUM GWs
data "openstack_networking_port_v2" "ipum-bmcs" {
  for_each = toset(var.assigned_ipums)
  name = "${each.key}bmc"
}

resource "openstack_networking_secgroup_rule_v2" "ssh-ipum-bmc" {
  for_each = data.openstack_networking_port_v2.ipum-bmcs

  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "${element(each.value.all_fixed_ips, 0)}/32"
  security_group_id = openstack_networking_secgroup_v2.ctrl-1g-bmc.id
}

resource "openstack_networking_secgroup_rule_v2" "https-ipum-bmc" {
  for_each = data.openstack_networking_port_v2.ipum-bmcs

  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = "${element(each.value.all_fixed_ips, 0)}/32"
  security_group_id = openstack_networking_secgroup_v2.ctrl-1g-bmc.id
}

resource "openstack_networking_secgroup_rule_v2" "icmp-ipum-bmc" {
  for_each = data.openstack_networking_port_v2.ipum-bmcs

  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "icmp"
  port_range_min    = 0
  port_range_max    = 0
  remote_ip_prefix  = "${element(each.value.all_fixed_ips, 0)}/32"
  security_group_id = openstack_networking_secgroup_v2.ctrl-1g-bmc.id
}

resource "openstack_networking_secgroup_rule_v2" "bmc-net-dns-tcp" {
  for_each = toset(["100", "101"])
  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 53
  port_range_max    = 53
  remote_ip_prefix = "10.1.255.${each.key}/32"
  security_group_id = openstack_networking_secgroup_v2.ctrl-1g-bmc.id
}

resource "openstack_networking_secgroup_rule_v2" "bmc-net-dns-udp" {
  for_each = toset(["100", "101"])
  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "udp"
  port_range_min    = 53
  port_range_max    = 53
  remote_ip_prefix = "10.1.255.${each.key}/32"
  security_group_id = openstack_networking_secgroup_v2.ctrl-1g-bmc.id
}

data "openstack_networking_network_v2" "ipum" {
  name = var.ipum_network_name
}

data "openstack_networking_subnet_v2" "bmc" {
  name       = var.bmc_subnet_name
  network_id = data.openstack_networking_network_v2.ipum.id
}

data "openstack_networking_subnet_v2" "gw" {
  name       = var.gw_subnet_name
  network_id = data.openstack_networking_network_v2.ipum.id
}

resource "openstack_networking_port_v2" "bmc" {
  name               = "${local.name}-bmc"
  network_id         = data.openstack_networking_network_v2.ipum.id
  admin_state_up     = "true"
  security_group_ids = [openstack_networking_secgroup_v2.ctrl-1g-bmc.id]
  fixed_ip {
    subnet_id = data.openstack_networking_subnet_v2.bmc.id
  }
}
resource "openstack_networking_port_v2" "gw" {
  name           = "${local.name}-gw"
  network_id     = data.openstack_networking_network_v2.ipum.id
  admin_state_up = "true"
  # TODO: is this needed to make multicast traffic flow in OK?
  # NH: enabled port security on this interface to allow controlling which IPUM GW interfaces can be accessed
  fixed_ip {
    subnet_id = data.openstack_networking_subnet_v2.gw.id
  }
  security_group_ids = [openstack_networking_secgroup_v2.ctrl-1g.id]
}

variable "rnic_profile" {
    type = map
    description = "Custom binding information, as terraform map"
    default = {
        capabilities = ["switchdev"]
    }
}

resource "openstack_networking_port_v2" "rnic" {
  name           = "${local.name}-rnic"
  network_id     = var.rnic_network_id
  admin_state_up = "true"
  binding {
    vnic_type = "direct"
    profile = jsonencode(var.rnic_profile)
  }
  fixed_ip {
    subnet_id = var.rnic_subnet_id
  }
  port_security_enabled = "false"
  # don't overrite os-vif adding chosen PCI device
  lifecycle {
    ignore_changes = [
      binding,
    ]
  }
}

resource "openstack_networking_port_v2" "storage" {
  name           = "${local.name}-storage"
  network_id     = var.storage_network_id
  admin_state_up = "true"
  fixed_ip {
    subnet_id = var.storage_subnet_id
  }
}

data "openstack_images_image_v2" "image" {
  name = var.image
}
data "openstack_compute_flavor_v2" "flavor" {
  name = var.flavor
}
data "template_file" "cloud-config-template" {
  template = file("${path.module}/templates/cloud-config.tpl")
  vars = {
    rnic_mac = openstack_networking_port_v2.rnic.mac_address
    storage_mac = openstack_networking_port_v2.storage.mac_address
    bmc_mac = openstack_networking_port_v2.bmc.mac_address
    gw_mac = openstack_networking_port_v2.gw.mac_address
    cidr_hint = var.cidr_hint
  }
}
resource "openstack_compute_instance_v2" "ctrl" {
  name            = local.name
  image_id        = data.openstack_images_image_v2.image.id
  flavor_id       = data.openstack_compute_flavor_v2.flavor.id
  key_pair        = var.key_pair
  config_drive    = true
  user_data = data.template_file.cloud-config-template.rendered
  lifecycle {
    prevent_destroy = true
  }
  network {
    port = openstack_networking_port_v2.ctrl.id
  }
  network {
    port = openstack_networking_port_v2.bmc.id
  }
  network {
    port = openstack_networking_port_v2.gw.id
  }
  network {
    port = openstack_networking_port_v2.rnic.id
  }
  network {
    port = openstack_networking_port_v2.storage.id
  }
}
