#cloud-config

mounts:
 - [ "10.12.${cidr_hint}.250:/public1", "/mnt/public", "nfs", "ro,relatime,vers=3,hard,proto=tcp,nconnect=16,timeo=600,retrans=2,sec=sys,mountvers=3,mountport=2049,mountproto=udp,rsize=524288,wsize=524288,namlen=255"]
 - [ "10.12.${cidr_hint}.250:/admin1", "/mnt/admin", "nfs", "rw,relatime,vers=3,hard,proto=tcp,nconnect=16,timeo=600,retrans=2,sec=sys,mountvers=3,mountport=2049,mountproto=udp,rsize=524288,wsize=524288,namlen=255"]


packages:
 - nfs-common
 - fio

write_files:
- content: |
    [Match]
    MACAddress=${bmc_mac}

    [Network]
    DHCP=yes

    [DHCPv4]
    UseDNS=false
  path: /etc/systemd/network/04-bmc.network
- content: |
    [Match]
    MACAddress=${gw_mac}

    [Network]
    DHCP=yes

    [DHCPv4]
    UseDNS=false
  path: /etc/systemd/network/05-gw.network
- content: |
    [Match]
    MACAddress=${rnic_mac}

    [Network]
    DHCP=yes

    [DHCPv4]
    UseDNS=false
  path: /etc/systemd/network/06-rnic.network
- content: |
    [Match]
    MACAddress=${storage_mac}

    [Network]
    DHCP=yes

    [DHCPv4]
    UseDNS=false
  path: /etc/systemd/network/07-storage.network
runcmd:
  - [systemctl, restart, systemd-networkd]